package exercises.iteration;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class FactorialTest {

  @ParameterizedTest(name = "{index}. Test {0}! = {1}")
  @CsvFileSource(resources = "/factorialDataSetSmall.csv")
  void testGetFactorialViaLoop(int input, int factorial) {
    assertEquals(Factorial.getFactorialViaLoop(input), factorial);
  }

  @ParameterizedTest(name = "{index}. Test {0}! = {1}")
  @CsvFileSource(resources = "/factorialDataSetSmall.csv")
  void testGetFactorialViaRecursion(int input, int factorial) {
    assertEquals(Factorial.getFactorialViaRecursion(input), factorial);
  }

  @ParameterizedTest(name = "{index}. Test {0}! = {1}")
  @CsvFileSource(resources = "/factorialDataSetSmall.csv")
  void testGetFactorialFunctionally(int input, int factorial) {
    assertEquals(Factorial.getFactorialFunctionally(input), factorial);
  }

  @ParameterizedTest(name = "{index}. Test {0}! = {1}")
  @CsvFileSource(resources = "/factorialDataSetLarge.csv")
  void testGetFactorial(int input, BigDecimal factorial) {
    Factorial.getFactorial(input).ifPresent(e -> assertEquals(e, factorial));
  }
}