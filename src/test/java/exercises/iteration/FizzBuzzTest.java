package exercises.iteration;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class FizzBuzzTest {

  @Test
  void testFizzBuzzViaStream() {
    assertEquals(FizzBuzz.fizzBuzzViaStream().filter(e -> e.equals("FizzBuzz")).count(), 6);
    assertEquals(FizzBuzz.fizzBuzzViaStream().filter(e -> e.equals("Fizz")).count(), 27);
    assertEquals(FizzBuzz.fizzBuzzViaStream().filter(e -> e.equals("Buzz")).count(), 14);
  }

  @Test
  void testFizzBuzzViaLoop() {
    var list = FizzBuzz.fizzBuzzViaLoop();
    assertEquals(list.stream().filter(e -> e.equals("FizzBuzz")).count(), 6);
    assertEquals(list.stream().filter(e -> e.equals("Fizz")).count(), 27);
    assertEquals(list.stream().filter(e -> e.equals("Buzz")).count(), 14);
  }

  @Test
  void testFizzBuzzViaRecursion() {
    var list = FizzBuzz.fizzBuzzViaRecursion(100, new ArrayList<>());
    assertEquals(list.stream().filter(e -> e.equals("FizzBuzz")).count(), 6);
    assertEquals(list.stream().filter(e -> e.equals("Fizz")).count(), 27);
    assertEquals(list.stream().filter(e -> e.equals("Buzz")).count(), 14);
  }
}