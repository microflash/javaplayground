package exercises.iteration;

import static org.junit.jupiter.api.Assertions.*;

import exercises.iteration.Fibonacci.Pair;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class FibonacciTest {

  private static List<Integer> list;
  private static List<Pair> stream;

  @BeforeAll
  static void setUp() {
    list = Fibonacci.fibonacciViaLoop(15);
    stream = Fibonacci.fibonacciViaStream(15).collect(Collectors.toList());
  }

  @ParameterizedTest(name = "{index}. Test F({0}) = {1}")
  @CsvFileSource(resources = "/fibonacciDataSet.csv")
  void testFibonacciViaLoop(Integer instance, Integer value) {
    assertEquals(list.get(instance - 1), value);
  }

  @ParameterizedTest(name = "{index}. Test F({0}) = {1}")
  @CsvFileSource(resources = "/fibonacciDataSet.csv")
  void testFibonacciViaStream(int instance, BigInteger value) {
    assertEquals(stream.get(instance - 1).previous, value);
  }

  @ParameterizedTest(name = "{index}. Test F({0}) = {1}")
  @CsvFileSource(resources = "/fibonacciDataSet.csv")
  void testRecursiveFibonacci(int instance, int value) {
    assertEquals(Fibonacci.recursiveFibonacci(instance), value);
  }
}