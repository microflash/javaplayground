package core.collections;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

  public static void main(String[] args) {
    var names = new ArrayList<String>();
    names.add("Jane");
    names.add("Patterson");
    names.add("Zapata");
    names.add("Mayfair");

    if (names instanceof List) {
      names.forEach(System.out::println);
    }
  }
}
