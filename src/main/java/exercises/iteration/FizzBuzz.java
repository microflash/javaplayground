package exercises.iteration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class FizzBuzz {

  static Stream<String> fizzBuzzViaStream() {
    return IntStream.rangeClosed(1, 100)
        .mapToObj(e -> e % 15 == 0 ? "FizzBuzz" : e % 3 == 0 ? "Fizz" : e % 5 == 0 ? "Buzz" : String.valueOf(e));
  }

  static List<String> fizzBuzzViaLoop() {
    var list = new ArrayList<String>();
    for (int i = 1; i <= 100; i++) {
      if (i % 15 == 0) {
        list.add("FizzBuzz");
      } else if (i % 3 == 0) {
        list.add("Fizz");
      } else if (i % 5 == 0) {
        list.add("Buzz");
      } else {
        list.add(String.valueOf(i));
      }
    }
    return list;
  }

  static List<String> fizzBuzzViaRecursion(int n, List<String> list) {
    if (n < 1) {
      Collections.reverse(list);
      return list;
    }
    list.add(n % 15 == 0 ? "FizzBuzz" : n % 3 == 0 ? "Fizz" : n % 5 == 0 ? "Buzz" : String.valueOf(n));
    return fizzBuzzViaRecursion(n - 1, list);
  }

}
