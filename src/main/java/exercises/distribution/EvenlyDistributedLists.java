package exercises.distribution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EvenlyDistributedLists {

  private static List<List<Integer>> split(final List<Integer> source, final int numberOfChunks) {

    final int sizeOfSmallSublists = source.size() / numberOfChunks;
    final int sizeOfLargeSublists = sizeOfSmallSublists + 1;
    final int numberOfLargeSublists = source.size() % numberOfChunks;
    final int numberOfSmallSublists = numberOfChunks - numberOfLargeSublists;

    List<List<Integer>> sublists = new ArrayList<>(numberOfChunks);
    int numberOfElementsHandled = 0;
    for (int i = 0; i < numberOfChunks; i++) {
      int size = i < numberOfSmallSublists ? sizeOfSmallSublists : sizeOfLargeSublists;
      List<Integer> sublist = source.subList(numberOfElementsHandled, numberOfElementsHandled + size);
      sublists.add(sublist);
      numberOfElementsHandled += size;
    }
    return sublists;
  }

  public static void main(String[] args) {
    split(IntStream.range(0, 27).boxed().collect(Collectors.toList()), 7).forEach(System.out::println);
  }
}
