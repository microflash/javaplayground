# Java Playground
A collection of Java problems and concepts

## Table of Contents

- [Factorial](./src/main/java/exercises/iteration/Factorial.java)
- [Fibonacci](./src/main/java/exercises/iteration/Fibonacci.java)
- [FizzBuzz](./src/main/java/exercises/iteration/FizzBuzz.java)

---
### Dependencies
- Java 10
- JUnit 5 "Jupiter"
- Surefire 2.21